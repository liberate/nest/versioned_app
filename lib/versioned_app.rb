require 'paper_trail'

module VersionedApp
  class Engine < ::Rails::Engine
    #
    # make db:migrate automatically pull in engine migrations:
    #
    initializer :append_migrations do |app|
      unless app.config.root.to_s =~ /test\/dummy/
        app.config.paths["db/migrate"] << config.paths["db/migrate"].first
      end
    end
  end
end
