This is a Rails Engine that handles integrating PaperTrail version tracking into
your application.

Features:

* Extended version table
* Admin UI for browsing changes
