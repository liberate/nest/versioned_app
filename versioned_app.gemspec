Gem::Specification.new do |spec|
  spec.name        = "versioned_app"
  spec.version     = "0.1.0"
  spec.authors     = ["Nest Developers"]
  spec.summary     = "Integrate versions into your Rails application"
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*"]
  end

  spec.add_dependency "paper_trail"
end
