class CreateVersions < ActiveRecord::Migration[7.1]
  def change
    create_table "versions", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.string "item_type", null: false
      t.string "item_subtype"
      t.integer "item_id", null: false
      t.string "event", null: false
      t.integer "whodunnit"
      t.text "object"
      t.text "object_changes"
      t.datetime "created_at"
      t.integer "domain_id"
      t.integer "user_id"
      t.text "data"
      t.text "change_message"
      t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
    end
  end
end
