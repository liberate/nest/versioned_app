#
# Custom version of PaperTrail::Version
#
class Version < PaperTrail::Version
  store :data

  belongs_to :user, optional: true
  belongs_to :domain, optional: true
  belongs_to :who, :class_name => 'User', :foreign_key => :whodunnit, optional: true

  def name
    type_name = item_type&.split("::")&.last&.downcase
    "change_%s_%s" % [type_name, event&.downcase]
  end

  def username
    user&.address || data["username"] || domain&.domain || "unknown"
  end

  def description(prefix: "")
    if change_message.present?
      change_message
    elsif changeset["memo"]&.last.present?
      changeset["memo"].last
    elsif event == "destroy"
      "Record Destroyed"
    elsif event == "create"
      "New Record"
    else
      prefix + " " + human_keys
    end
  end

  # the fields that changed, in human readable format
  def human_keys
    (changeset.keys - ["updated_at"]).map{|k| k.humanize}.sort.join(', ')
  end
end
