class VersionsController < AdminApp::ApplicationController
  before_action :set_version, only: [:show, :update]

  layout :choose_layout

  def index
    @versions = Version.page(params[:page]).order("created_at DESC")
  end

  def show
    # we load the objects, and not just the changeset, so that we can
    # properly display enums and associations in the view. however,
    # the last version does not have an after_record, so we use the current record.
    @before_record = @version.reify
    if @version.next
      @after_record = @version.next.reify
    else
      # note: @version.reify overwrites @version.item, so we have to
      # fetch it a different way.
      current_record = @version.item&.class&.find_by_id(@version.item_id)
      if current_record&.versions&.last == @before_record
        # only include @after_record based on item (current_record)
        # if @before_record is the last version
        @after_record = current_record
      else
        nil
      end
    end
  end

  def update
    if @version.update(version_params)
      redirect_to version_path(@version, owner: params[:owner]), notice: "Record saved"
    else
      render :show
    end
  end

  private

  def choose_layout
    if params[:owner]
      'center'
    else
      'application'
    end
  end

  def set_version
    @version = PetalVersion.find(params[:id])
  end

  def version_params
    params.require(:paper_trail_version).permit(:change_message)
  end
end
