module VersionHelper
  protected

  def version_badge(version)
    content_tag(:span, class: "badge bg-" + version_color(version)) do
      if version.change_message?
        content_tag(:span, version.event, class: 'pr-1') + content_tag(:i, nil, class: "fa fa-fw fa-bullhorn")
      else
        version.event
      end
    end
  end

  def version_color(version)
    case version.event
      when "create" then "success"
      when "update" then "info"
      when "destroy" then "danger"
    end
  end
end